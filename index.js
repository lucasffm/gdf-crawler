import axios from 'axios';
import database from './src/models/db';
import Imovel from './src/models/Imovel';

let inscricao = 14022656;
let limit = 14022800;
let inscricaoStr = '';

function generateNumber() {
  let result = inscricao.toString();
  inscricao += 1;
  if (result.length < 8) {
    result = result.padStart(8, '0');
  }

  if (result.split('').pop() == 0) {
    inscricaoStr = result.slice(0, -1) + 'X';
    return;
  }
  if (inscricaoStr.split('').pop() == 'X') {
    inscricao -= 1;
    inscricaoStr = result.slice(0, -1) + '0';
    return;
  }

  inscricaoStr = result;
}

async function saveData(data) {
  const alreadyExist = await Imovel.findOne({
    where: { InscricaoImovel: data.InscricaoImovel },
  });

  if (!alreadyExist) {
    await Imovel.create(data);
  }
}

async function fetchData() {
  generateNumber();
  console.log(inscricaoStr);
  try {
    const { data } = await axios.post(
      'https://ww1.receita.fazenda.df.gov.br/WPI/psv/Consulta/Imovel/FichaCadastral',
      { InscricaoImovel: inscricaoStr }
    );

    if (data) {
      await saveData(data);
      results.push(data);
    }
    if (inscricao < limit) {
      await fetchData();
    }
    return results;
  } catch (error) {
    if (inscricao < limit) {
      await fetchData();
    }
  }
}

database.sync().then(async (result) => {
  console.log('DB INICIADO');

  await fetchData();
});
